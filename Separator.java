import java.util.Scanner;

public class Separator {
   
   public static final int N = 5;

   public static void main(String[] args)
   {
      Scanner input = new Scanner(System.in);
      
      int[] integers = new int[N];
      float[] floats = new float[N];
      int int_count = 0;
      int float_count = 0;
      boolean invalid_char = false;
      boolean maxed = false;
      
      System.out.print("Please input numbers: ");
      while(invalid_char == false && input.hasNext() && maxed == false)
      {
         if(input.hasNextInt())
         {
            if(int_count < N)
            {
               integers[int_count] = input.nextInt();
               int_count++;
            }
            else
               maxed = true;
               
         }
         else if(input.hasNextFloat())
         {
            if(float_count < N)
            {
               floats[float_count] = input.nextFloat();
               float_count++;
            }
            else
               maxed = true;
         }
         else
            invalid_char = true;
      }
      
      System.out.print("\nIntegers: ");
      if(int_count > 0)
      {
         for(int i=0; i<int_count ; i++)
            System.out.print(integers[i] + " ");
      }
      System.out.println();
      System.out.print("Floats: ");
      if(float_count > 0)
      {
         for(int j=0; j<float_count ; j++)
            System.out.print(floats[j] + " ");
      }
      System.out.println();
   }
   
}
